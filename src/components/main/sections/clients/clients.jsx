import React from 'react';
import Slider from 'react-slick';
import clients from '../../../../data/clients.json';
import styles from './clients.module.scss';
import sliderSettings from './slider-settings';

const Clients = () => {
  return (
    <section className={styles.clients}>
      <div className={styles['clients__section-triangle']} />
      <div className="container">
        <div className="row">
          <div className={styles['clients__experience-with']}>
            <h3>Experience with</h3>
          </div>
          <div className={styles.clients__companies}>
            <Slider {...sliderSettings}>
              {clients.map((c, i) => (
                <div key={i}>
                  <img
                    className={styles['clients__company-logo']}
                    src={require(`../../../../assets/images/logo/${c.imgSrc}`)}
                    alt={c.name}
                  />
                  <h4>{c.name}</h4>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Clients;
