import classNames from 'classnames';
import React, { Fragment, useEffect, useState } from 'react';
import PreloaderLoading from './preloader-loading/preloader-loading';
import styles from './preloader.module.scss';

const Preloader = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    const disablePreloader = () => {
      setTimeout(() => {
        setIsDisabled(true);
      }, 500);
    };

    setTimeout(() => {
      setIsLoaded(true);
      disablePreloader();
    }, 1000);
  }, []);

  return (
    <Fragment>
      {!isDisabled ? (
        <div
          className={classNames(styles.preloader, isLoaded ? styles['preloader--fadeOut'] : null)}
        >
          <div className={styles.preloader__loading}>
            <PreloaderLoading />
          </div>
        </div>
      ) : null}
    </Fragment>
  );
};

export default Preloader;
