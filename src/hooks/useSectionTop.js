import { useLayoutEffect, useRef } from 'react';

const useSectionTop = onComponentRendered => {
  const ref = useRef();

  useLayoutEffect(() => {
    const triggerComponentTop = () => {
      onComponentRendered(ref.current.getBoundingClientRect().top);
    };

    triggerComponentTop();

    window.addEventListener('resize', triggerComponentTop);

    return () => {
      window.removeEventListener('resize', triggerComponentTop);
    };
  }, [onComponentRendered]);

  return [ref];
};

export default useSectionTop;
