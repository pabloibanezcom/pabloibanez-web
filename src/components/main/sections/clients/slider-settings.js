const sliderSettings = {
  dots: true,
  arrows: false,
  autoplay: true,
  infinite: true,
  speed: 400,
  autoplaySpeed: 5000,
  dotsClass: 'slick-dots dots-white',
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

export default sliderSettings;
