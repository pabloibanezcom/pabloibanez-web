export { default as Clients } from './clients/clients';
export { default as Contact } from './contact/contact';
export { default as Portfolio } from './portfolio/portfolio';
export { default as Resume } from './resume/resume';
export { default as SimpleIntro } from './simple-intro/simple-intro';
export { default as SkillsFinder } from './skills-finder/skills-finder';
