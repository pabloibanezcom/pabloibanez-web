const fs = require('fs');

const environmentCompile = () => {
  const envPrefixes = {
    production: 'PROD',
    staging: 'STG',
    development: 'DEV'
  };

  // Get environment
  let environment = JSON.parse(process.env.npm_config_argv)
    .original.slice(-1)[0]
    .replace('--', '');
  environment = ['production', 'staging', 'development'].includes(environment) ? environment : null;

  if (!environment) {
    process.env.NODE_ENV = 'development';
    require('../config/env');
  } else {
    Object.keys(process.env).forEach(envName => {
      if (envName.startsWith(envPrefixes[environment])) {
        process.env[envName.replace(`${envPrefixes[environment]}_`, '')] = process.env[envName];
      }
    });
  }

  if (environment) {
    console.log(`Environment name: ${environment}`);
  }

  fs.writeFileSync('./src/environment.json', JSON.stringify(process.env, null, 2));
};

environmentCompile();
