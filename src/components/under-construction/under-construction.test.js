import { shallow } from 'enzyme';
import React from 'react';
import UnderConstruction from './under-construction';

describe('<UnderConstruction />', () => {
  let wrapper;

  it('renders <UnderConstruction /> component', () => {
    wrapper = shallow(<UnderConstruction />);
    expect(wrapper).toMatchSnapshot();
  });
});
