import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-scroll';
import styles from './back-to-top.module.scss';

const BackToTop = ({ show }) => {
  return (
    <Link
      type="button"
      to="main"
      spy
      smooth
      offset={-70}
      duration={500}
      className={classNames(styles['back-to-top'], show ? styles['back-to-top__shown'] : null)}
    >
      <i className="icon-chevron-up" />
    </Link>
  );
};

BackToTop.propTypes = {
  show: PropTypes.bool
};

BackToTop.defaultProps = {
  show: false
};

export default BackToTop;
