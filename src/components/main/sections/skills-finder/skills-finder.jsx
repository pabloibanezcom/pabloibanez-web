import React, { useState } from 'react';
import skills from '../../../../data/skills.json';
import styles from './skills-finder.module.scss';

const minStrLength = 2;

const SkillsFinder = () => {
  const [inputStr, setInputStr] = useState('');
  const [suggestions, setSuggestions] = useState([]);
  const [selectedSkill, setSelectedSkill] = useState(null);

  const searchSkill = str => {
    return skills.filter(s => s.name.toLowerCase().includes(str.toLowerCase()));
  };

  const handleOnChange = evt => {
    setInputStr(evt.target.value);
    setSuggestions(evt.target.value.length >= minStrLength ? searchSkill(evt.target.value) : []);
  };

  const handleSuggestionSelected = suggestion => {
    setInputStr(suggestion.name);
    setSelectedSkill(suggestion);
    setSuggestions([]);
  };

  return (
    <section className={styles['skills-finder']}>
      <div className="container">
        <h2>Skills finder</h2>
        <h4>Find out my experience on a certain technology</h4>
        <div className={styles['skills-finder__search-container']}>
          <div className={styles['skills-finder__search-input-wrapper']}>
            <input
              type="text"
              className={styles['skills-finder__search-input']}
              placeholder="Type a technology..."
              value={inputStr}
              onChange={handleOnChange}
            />
            <ul className={styles['skills-finder__search-suggestions']}>
              {suggestions.map(suggestion => (
                <li key={suggestion.id} className={styles['skills-finder__search-suggestion']}>
                  <a onClick={() => handleSuggestionSelected(suggestion)}>
                    <img
                      alt={suggestion.name}
                      src={require(`../../../../assets/images/skills/${suggestion.id}.png`)}
                      className={styles['skills-finder__suggestion-img']}
                    />
                    {suggestion.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
        {selectedSkill ? (
          <div className={styles['skills-finder__selected-skill']}>{selectedSkill.name}</div>
        ) : null}
      </div>
    </section>
  );
};

export default SkillsFinder;
