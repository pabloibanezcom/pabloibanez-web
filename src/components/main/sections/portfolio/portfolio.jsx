import React from 'react';
import { Link } from 'react-scroll';
import portfolio from '../../../../data/portfolio.json';
import styles from './portfolio.module.scss';

const Portfolio = () => {
  return (
    <section className={styles.portfolio}>
      <div className="container">
        <div className="row">
          <div className={styles['portfolio__worked-on']}>
            <h3>Some popular sites I&apos;ve worked on</h3>
          </div>
          <div className={styles.portfolio__sites}>
            <div className="row justify-content-center">
              {portfolio.commercial.map((site, i) => (
                <div key={i} className={styles['portfolio__site-wrapper']}>
                  <div className={styles.portfolio__site}>
                    <div className={styles['portfolio__site__image-wrapper']}>
                      <div className={styles['portfolio__site__info-box']}>
                        <div className={styles.portfolio__site__info}>
                          <div>
                            <img
                              src={require(`../../../../assets/images/logo/${site.logoImgSrc}`)}
                              alt={site.name}
                            />
                          </div>
                          <div className={styles['portfolio__site__info-right']}>
                            {site.description}
                            <div>
                              <Link
                                to={site.resumeSection}
                                spy
                                smooth
                                offset={-70}
                                duration={500}
                                className={styles.portfolio__site__button}
                              >
                                <i className="icon-briefcase" />
                                See experience
                              </Link>
                              <a
                                href={site.url}
                                target="_blank"
                                rel="noopener noreferrer"
                                className={styles.portfolio__site__link}
                              >
                                <i className="icon-earth" />
                                Go to site
                              </a>
                            </div>
                          </div>
                        </div>
                        <div className={styles.portfolio__site__overlay} />
                      </div>
                      <img
                        className={styles.portfolio__site__thumb}
                        src={require(`../../../../assets/images/sites/${site.imgSrc}`)}
                        alt={site.name}
                      />
                    </div>
                    <h4>{site.name}</h4>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Portfolio;
